<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * TODO: Add the double asterisk in the line above for this route to work
     * @Route("/", name="home")
     */
    public function index(Request $request)
    {
        $result = '';

        /* File system IO speed */
        $start = microtime(true);
        for ($i=0; $i<1000; $i++) {
            $file = fopen(__DIR__, "r");
        }
        $time = microtime(true) - $start;
        $result .= "1: " . $time;

        /* Network speed */
        $start = microtime(true);
        for ($i=0; $i<3; $i++) {
            $file = file_get_contents("http://www.google.com");
        }
        $time = microtime(true) - $start;
        $result .= "<br/>2: " . $time;

        /* PHP execution speed */
        $start = microtime(true);
        for ($i=0; $i<10; $i++) {
            $a = array_fill(0, 1000, mt_rand(0, 1000));
            $b = array_fill(0, 1000, mt_rand(0, 1000));
            $c = [];
            foreach ($a as $h) {
                foreach ($b as $j) {
                    $c[] = $h*$j;
                }
            }
        }
        $time = microtime(true) - $start;
        $result .= "<br/>3: " . $time;


        return new Response($result);
    }
}
